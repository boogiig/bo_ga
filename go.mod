module boga.me

go 1.17

require (
	github.com/go-sql-driver/mysql v1.6.0
	golang.org/x/oauth2 v0.0.0-20220223155221-ee480838109b
)

require (
	github.com/coreos/go-oidc/v3 v3.1.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/square/go-jose.v2 v2.6.0 // indirect
)
