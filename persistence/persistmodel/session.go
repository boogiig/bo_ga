package persistmodel

import "time"

// Session represents a bo_ga_session storage entity.
type Session struct {
	SessionID string
	UserID    int64
	Expiry    time.Time
}
