package persistmodel

// User represents a bo_ga_user storage entity.
type User struct {
	ID        int64  `json:"id"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	Fullname  string `json:"fullname"`
	Telephone string `json:"telephone"`
	ProfileOk bool   `json:"-"`
	IsOauth   bool   `json:"-"`
}
