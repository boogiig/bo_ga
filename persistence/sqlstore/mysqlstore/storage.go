package mysqlstore

import (
	"context"
	"database/sql"
)

// mySQLStorage represents a SQL storage.
type mySQLStorage struct {
	// DB represents a MySQL database handler.
	db *sql.DB
}

func newStorage(db *sql.DB) *mySQLStorage {
	return &mySQLStorage{db: db}
}

func (s *mySQLStorage) ExecTransaction(ctx context.Context, f func(tx *sql.Tx) error) error {
	tx, txErr := s.db.BeginTx(ctx, nil)
	if txErr != nil {
		return txErr
	}
	if err := f(tx); err != nil {
		_ = tx.Rollback()
		return err
	}
	return tx.Commit()
}
