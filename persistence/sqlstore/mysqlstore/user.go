package mysqlstore

import (
	"context"
	"database/sql"
	"errors"

	"boga.me/common/errorc"
	"boga.me/common/logc"
	"boga.me/persistence/persistmodel"
	"github.com/go-sql-driver/mysql"
)

type mySQLUser struct {
	*mySQLStorage
}

func newUser(db *sql.DB) *mySQLUser {
	return &mySQLUser{
		mySQLStorage: newStorage(db),
	}
}

// CreateUserByEmailPassword inserts a new user entity into storage
func (u *mySQLUser) CreateUserByEmailPassword(ctx context.Context, email string, password string, profileOk bool, isOauth bool) (*persistmodel.User, error) {
	exists, err := u.UserExists(ctx, email)
	if err != nil {
		return nil, errorc.ErrUserRegistrationFailed
	}
	if exists {
		return nil, errorc.ErrUserAlreadyRegistered
	}
	query := "INSERT INTO bo_ga_user(email, password, profile_ok, is_oauth) VALUES (?, ?, ?, ?)"

	res, err := u.db.ExecContext(ctx, query, email, password, profileOk, isOauth)

	if err != nil {
		logc.Errorf("CreateUser error: %v", err)
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			return nil, errorc.ErrUserAlreadyRegistered
		}
		return nil, errorc.ErrUserRegistrationFailed
	}

	lastId, err := res.LastInsertId()

	if err != nil {
		logc.Errorf("CreateUser LastInsertId error: %v", err)
		return nil, errorc.ErrUserRegistrationFailed
	}

	return &persistmodel.User{ID: lastId, Email: email, Password: password, ProfileOk: profileOk, IsOauth: isOauth}, nil
}

// CreateUserByEmailName inserts a new user entity into storage
func (u *mySQLUser) CreateUserByEmailName(ctx context.Context, email string, fullname string, profileOk bool, isOauth bool) (*persistmodel.User, error) {
	exists, err := u.UserExists(ctx, email)
	if err != nil {
		return nil, errorc.ErrUserRegistrationFailed
	}
	if exists {
		return nil, errorc.ErrUserAlreadyRegistered
	}
	query := "INSERT INTO bo_ga_user(email, fullname, profile_ok, is_oauth) VALUES (?, ?, ?, ?)"

	res, err := u.db.ExecContext(ctx, query, email, fullname, profileOk, isOauth)

	if err != nil {
		logc.Errorf("CreateUser error: %v", err)
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			return nil, errorc.ErrUserAlreadyRegistered
		}
		return nil, errorc.ErrUserRegistrationFailed
	}

	lastId, err := res.LastInsertId()

	if err != nil {
		logc.Errorf("CreateUser LastInsertId error: %v", err)
		return nil, errorc.ErrUserRegistrationFailed
	}

	return &persistmodel.User{ID: lastId, Email: email, Fullname: fullname, ProfileOk: profileOk, IsOauth: isOauth}, nil
}

// UpdateUser updates a user entity storage
func (u *mySQLUser) UpdateUser(ctx context.Context, user *persistmodel.User) error {
	query := "UPDATE bo_ga_user SET email=?, fullname=?, telephone=?, profile_ok=? WHERE id=?"

	res, err := u.db.ExecContext(ctx, query, user.Email, user.Fullname, user.Telephone, user.ProfileOk, user.ID)

	if err != nil {
		logc.Errorf("UpdateUser error: %v", err)
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			return errorc.ErrUserEmailDuplicated
		}
		return errorc.ErrUserProfileEditFailed
	}
	rows, err := res.RowsAffected()
	if err != nil {
		logc.Errorf("UpdateUser RowsAffected error: %v", err)
		return errorc.ErrUserProfileEditFailed
	}
	if rows < 1 {
		return errorc.ErrUserNotFoundByID
	}
	return nil
}

// UpdateUserProfileOk updates a user profile ok storage
func (u *mySQLUser) UpdateUserProfileOk(ctx context.Context, user *persistmodel.User) error {
	query := "UPDATE bo_ga_user SET profile_ok=? WHERE id=?"

	res, err := u.db.ExecContext(ctx, query, user.ProfileOk, user.ID)

	if err != nil {
		logc.Errorf("UpdateUserProfileOk error: %v", err)
		return errorc.ErrUserProfileEditFailed
	}
	rows, err := res.RowsAffected()
	if err != nil {
		logc.Errorf("UpdateUserProfileOk RowsAffected error: %v", err)
		return errorc.ErrUserProfileEditFailed
	}
	if rows < 1 {
		return errorc.ErrUserNotFoundByID
	}
	return nil
}

// DeleteUser deletes a user entity from storage.
func (u *mySQLUser) DeleteUser(ctx context.Context, userID int64) error {
	return nil
}

// FetchUserByID retrieves a user entity from storage.
func (u *mySQLUser) FetchUserByID(ctx context.Context, userID int64) (*persistmodel.User, error) {
	query := `SELECT email,IFNULL(password,"") as password,IFNULL(fullname,"") as fullname,IFNULL(telephone,"") as telephone,profile_ok,is_oauth FROM bo_ga_user WHERE id = ?`

	row := u.db.QueryRowContext(ctx, query, userID)
	user := &persistmodel.User{ID: userID}
	err := row.Scan(&user.Email, &user.Password, &user.Fullname, &user.Telephone, &user.ProfileOk, &user.IsOauth)
	if err != nil {
		logc.Errorf("FetchUserByID error: %v", err)
		if err == sql.ErrNoRows {
			return nil, errorc.ErrUserNotFoundByID
		}
		return nil, errorc.ErrSystem
	}
	return user, nil
}

// FetchUserByEmail retrieves a user entity from storage.
func (u *mySQLUser) FetchUserByEmail(ctx context.Context, email string) (*persistmodel.User, error) {
	query := `SELECT id,IFNULL(password,"") as password,IFNULL(fullname,"") as fullname,IFNULL(telephone,"") as telephone,profile_ok,is_oauth FROM bo_ga_user WHERE email = ?`

	row := u.db.QueryRowContext(ctx, query, email)
	user := &persistmodel.User{Email: email}
	err := row.Scan(&user.ID, &user.Password, &user.Fullname, &user.Telephone, &user.ProfileOk, &user.IsOauth)
	if err != nil {
		logc.Errorf("FetchUserByEmail error: %v", err)
		if err == sql.ErrNoRows {
			return nil, errorc.ErrUserNotFound
		}
		return nil, errorc.ErrSystem
	}
	return user, nil
}

// UserExists tells whether or not a user exists within storage.
func (u *mySQLUser) UserExists(ctx context.Context, email string) (bool, error) {
	query := "SELECT COUNT(id) uc FROM bo_ga_user WHERE email = ?"

	row := u.db.QueryRowContext(ctx, query, email)
	var count int
	err := row.Scan(&count)
	if err != nil {
		logc.Errorf("UserExists error: %v", err)
		return false, errorc.ErrSystem
	}
	return count > 0, nil
}
