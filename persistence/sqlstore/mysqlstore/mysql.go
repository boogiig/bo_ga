package mysqlstore

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"boga.me/common/logc"
	"boga.me/persistence/sqlstore/sqlrepository"
	_ "github.com/go-sql-driver/mysql"
)

type mySQLContainer struct {
	db      *sql.DB
	doneCh  chan chan bool
	user    *mySQLUser
	session *mySQLSession
}

func New(cfg *sqlrepository.Config) (sqlrepository.Container, error) {
	setupDB(cfg)
	var err error
	c := &mySQLContainer{doneCh: make(chan chan bool, 1)}

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true", cfg.User, cfg.Password, cfg.Host, cfg.Port, cfg.Database)
	c.db, err = sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	c.db.SetMaxOpenConns(cfg.PoolSize) // set max opened connection count

	if err := c.db.Ping(); err != nil {
		return nil, err
	}
	go c.loop()

	c.user = newUser(c.db)
	c.session = newSession(c.db)

	return c, nil
}

func NewDefaultConfig() *sqlrepository.Config {
	config := sqlrepository.NewDefaultConfig()
	config.Type = "MYSQL"
	config.Host = "localhost"
	config.Port = "3306"
	config.User = "root"
	config.Password = "root"
	config.Database = "bogadb"
	return config
}

func (c *mySQLContainer) Close(ctx context.Context) error {
	ch := make(chan bool)
	c.doneCh <- ch
	select {
	case <-ch:
		return nil
	case <-ctx.Done():
		return ctx.Err()
	}
}

func (c *mySQLContainer) loop() {
	tc := time.NewTicker(time.Second * 5)
	defer tc.Stop()

	for {
		select {
		case <-tc.C:
			if err := c.db.Ping(); err != nil {
				logc.Errorf("DB ping error: %v", err)
			}
		case ch := <-c.doneCh:
			if err := c.db.Close(); err != nil {
				logc.Errorf("DB close error: %v", err)
			}
			close(ch)
			return
		}
	}
}

// ExecTransaction :
func (c *mySQLContainer) ExecTransaction(ctx context.Context, f func(tx *sql.Tx) error) error {
	tx, txerr := c.db.BeginTx(ctx, nil)

	if txerr != nil {
		return txerr
	}
	if err := f(tx); err != nil {
		_ = tx.Rollback()
		return err
	}
	tx.Commit()
	return nil
}

// GetDB :
func (c *mySQLContainer) GetDB() *sql.DB {
	return c.db
}

func (c *mySQLContainer) User() sqlrepository.User       { return c.user }
func (c *mySQLContainer) Session() sqlrepository.Session { return c.session }

func setupDB(cfg *sqlrepository.Config) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/", cfg.User, cfg.Password, cfg.Host, cfg.Port)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		logc.Panicf("DB connection error: %v", err)
	}
	defer db.Close()

	_, err = db.Exec(fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %v CHARACTER SET UTF8", cfg.Database))
	if err != nil {
		logc.Panicf("CREATE DATABASE error: %v", err)
	}

	_, err = db.Exec("USE " + cfg.Database)
	if err != nil {
		logc.Panicf("USE error: %v", err)
	}

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS bo_ga_user (
		id                  BIGINT NOT NULL AUTO_INCREMENT,
		email               VARCHAR(100) NOT NULL,
		password            VARCHAR(255),
		fullname            VARCHAR(255),
		telephone           VARCHAR(255),
		profile_ok          BOOLEAN NOT NULL,
		is_oauth         	BOOLEAN NOT NULL,
		PRIMARY KEY(id),
		UNIQUE INDEX i_user_email (email)
	) ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;`)
	if err != nil {
		logc.Panicf("CREATE schema error: %v", err)
	}

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS bo_ga_session (
		session_id          VARCHAR(100) NOT NULL,
		user_id            	BIGINT NOT NULL,
		expiry            	TIMESTAMP NOT NULL,
		PRIMARY KEY(session_id)
	) ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;`)
	if err != nil {
		logc.Panicf("CREATE schema error: %v", err)
	}
}
