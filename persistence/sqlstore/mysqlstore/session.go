package mysqlstore

import (
	"context"
	"database/sql"
	"errors"
	"time"

	"boga.me/common/errorc"
	"boga.me/common/logc"
	"boga.me/persistence/persistmodel"
	"github.com/go-sql-driver/mysql"
)

type mySQLSession struct {
	*mySQLStorage
}

func newSession(db *sql.DB) *mySQLSession {
	return &mySQLSession{
		mySQLStorage: newStorage(db),
	}
}

// CreateSession inserts a new session entity into storage
func (s *mySQLSession) CreateSession(ctx context.Context, sessionID string, userID int64, expiry time.Time) error {
	query := "INSERT INTO bo_ga_session(session_id, user_id, expiry) VALUES (?, ?, ?)"

	_, err := s.db.ExecContext(ctx, query, sessionID, userID, expiry)

	if err != nil {
		logc.Errorf("CreateSession error: %v", err)
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			return errorc.ErrSessionDuplicated
		}
		return errorc.ErrSystem
	}

	return nil
}

// DeleteSession deletes a session entity from storage.
func (s *mySQLSession) DeleteSession(ctx context.Context, sessionID string) error {
	query := "DELETE FROM bo_ga_session WHERE session_id=?"

	_, err := s.db.ExecContext(ctx, query, sessionID)

	if err != nil {
		logc.Errorf("DeleteSession error: %v", err)
		return errorc.ErrSystem
	}
	return nil
}

// FetchSession retrieves a session entity from storage.
func (s *mySQLSession) FetchSession(ctx context.Context, sessionID string) (*persistmodel.Session, error) {
	query := "SELECT user_id,expiry FROM bo_ga_session WHERE session_id = ?"

	row := s.db.QueryRowContext(ctx, query, sessionID)
	session := &persistmodel.Session{SessionID: sessionID}
	err := row.Scan(&session.UserID, &session.Expiry)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errorc.ErrSessionNotFound
		}
		logc.Errorf("FetchSession error: %v", err)
		return nil, errorc.ErrSystem
	}
	if time.Now().UTC().After(session.Expiry.UTC()) {
		return nil, nil
	}
	return session, nil
}

// DeleteExpiredSessions deletes a expired sessions from storage.
func (s *mySQLSession) DeleteExpiredSessions(ctx context.Context) error {
	query := "DELETE FROM bo_ga_session WHERE UTC_TIMESTAMP(6) > expiry"

	_, err := s.db.ExecContext(ctx, query)

	if err != nil {
		return err
	}
	return nil
}
