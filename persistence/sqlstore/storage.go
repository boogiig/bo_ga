package sqlstore

import (
	"fmt"

	"boga.me/persistence/sqlstore/mysqlstore"
	"boga.me/persistence/sqlstore/sqlrepository"
)

func New(config *sqlrepository.Config) (sqlrepository.Container, error) {
	switch config.Type {
	case "MYSQL":
		return mysqlstore.New(config)
	default:
		return nil, fmt.Errorf("storage: unrecognized storage type: %s", config.Type)
	}
}

func NewDefaultConfig(Type string) *sqlrepository.Config {
	switch Type {
	case "MYSQL":
		return mysqlstore.NewDefaultConfig()
	default:
		return nil
	}
}
