package sqlrepository

import (
	"context"
	"time"

	"boga.me/persistence/persistmodel"
)

// Session defines session repository operations
type Session interface {
	// CreateSession inserts a new session entity into storage
	CreateSession(ctx context.Context, sessionID string, userID int64, expiry time.Time) error

	// DeleteSession deletes a session entity from storage.
	DeleteSession(ctx context.Context, sessionID string) error

	// FetchSession retrieves a session entity from storage.
	FetchSession(ctx context.Context, sessionID string) (*persistmodel.Session, error)

	// DeleteExpiredSessions retrieves a session entity from storage.
	DeleteExpiredSessions(ctx context.Context) error
}
