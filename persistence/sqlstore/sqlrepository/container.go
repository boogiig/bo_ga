package sqlrepository

import (
	"context"
	"database/sql"
)

type Container interface {
	// Close closes underlying storage resources, commonly shared across repositories.
	Close(ctx context.Context) error

	// ExecTransaction :
	ExecTransaction(ctx context.Context, f func(tx *sql.Tx) error) error

	// GetDB :
	GetDB() *sql.DB

	User() User
	Session() Session
}
