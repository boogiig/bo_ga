package sqlrepository

const defaultPoolSize = 16
const defaultMaxIdleSize = 8
const defaultSSLMode = "disable"

type Config struct {
	Type           string `yaml:"type"`
	Host           string `yaml:"host"`
	Port           string `yaml:"port"`
	User           string `yaml:"user"`
	Password       string `yaml:"password"`
	Database       string `yaml:"database"`
	PoolSize       int    `yaml:"pool_size"`
	MaxIdleSize    int    `yaml:"max_idle_size"`
	SSLMode        string `yaml:"ssl_mode"`
	ConnectTimeOut string `yaml:"connect_timeout"`
}

func NewDefaultConfig() *Config {
	return &Config{
		PoolSize:    defaultPoolSize,
		MaxIdleSize: defaultMaxIdleSize,
		SSLMode:     defaultSSLMode,
	}
}

func (c *Config) UnmarshalYAML(unmarshal func(interface{}) error) error {
	parsed := NewDefaultConfig()
	if err := unmarshal(&parsed); err != nil {
		return err
	}
	*c = Config(*parsed)

	return nil
}
