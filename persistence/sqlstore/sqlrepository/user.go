package sqlrepository

import (
	"context"

	"boga.me/persistence/persistmodel"
)

// User defines user repository operations
type User interface {
	// CreateUserByEmailPassword inserts a new user entity into storage
	CreateUserByEmailPassword(ctx context.Context, email string, password string, profileOk bool, isOauth bool) (*persistmodel.User, error)

	// CreateUserByEmailName inserts a new user entity into storage
	CreateUserByEmailName(ctx context.Context, email string, fullname string, profileOk bool, isOauth bool) (*persistmodel.User, error)

	// UpdateUser updates a user entity storage
	UpdateUser(ctx context.Context, user *persistmodel.User) error

	// UpdateUserProfileOk updates a user entity storage
	UpdateUserProfileOk(ctx context.Context, user *persistmodel.User) error

	// DeleteUser deletes a user entity from storage.
	DeleteUser(ctx context.Context, userID int64) error

	// FetchUserByID retrieves a user entity from storage.
	FetchUserByID(ctx context.Context, userID int64) (*persistmodel.User, error)

	// FetchUserByEmail retrieves a user entity from storage.
	FetchUserByEmail(ctx context.Context, email string) (*persistmodel.User, error)

	// UserExists tells whether or not a user exists within storage.
	UserExists(ctx context.Context, email string) (bool, error)
}
