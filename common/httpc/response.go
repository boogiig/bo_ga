package httpc

import (
	"encoding/json"
	"net/http"
)

func Response(w http.ResponseWriter, r *http.Request, status int, result string, message string) {
	jsonBody, _ := json.Marshal(map[string]string{
		result: message,
	})

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(jsonBody)
}

func ResponseDone(w http.ResponseWriter, r *http.Request, status int, message string) {
	Response(w, r, status, "success", message)
}

func ResponseError(w http.ResponseWriter, r *http.Request, status int, message string) {
	Response(w, r, status, "error", message)
}
