package callerutil

import (
	"path"
	"path/filepath"
	"runtime"
	"strings"

	"boga.me/common/sysglobal"
)

// CallerInfo :
type CallerInfo struct {
	Pkg      string
	Filename string
	Line     int
}

// GetCallerInfo :
func GetCallerInfo() CallerInfo {
	return GetCallerInfoSkip(3)
}

// GetCallerInfoSkip :
func GetCallerInfoSkip(skip int) CallerInfo {
	ci := CallerInfo{}
	_, file, ln, ok := runtime.Caller(skip)
	if ok {
		ci.Pkg = filepath.Base(path.Dir(file))
		if ci.Pkg == sysglobal.GlProcessName {
			ci.Pkg = ""
		}
		filename := filepath.Base(file)
		ci.Filename = strings.TrimSuffix(filename, filepath.Ext(filename))
		ci.Line = ln
	} else {
		ci.Filename = "???"
		ci.Pkg = "???"
	}
	return ci
}
