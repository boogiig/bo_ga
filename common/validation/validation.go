package validation

import (
	"net"
	"regexp"
	"strings"

	"boga.me/common/errorc"
)

func ValidateUserParams(email string, password string) error {
	if !IsEmailValid(email, false) {
		return errorc.ErrUserEmailNotValid
	}
	if !IsPasswordValid(password) {
		return errorc.ErrUserPasswordNotValid
	}
	return nil
}

func ValidateUserProfileParams(fullname string, telephone string) error {
	if !IsFullnameValid(fullname) {
		return errorc.ErrUserFullnameNotValid
	}
	if !IsTelephoneValid(telephone) {
		return errorc.ErrUserTelephoneNotValid
	}
	return nil
}

var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

func IsEmailValid(email string, lookupMX bool) bool {
	if len(email) < 3 && len(email) > 99 {
		return false
	}
	if !emailRegex.MatchString(email) {
		return false
	}
	if lookupMX {
		parts := strings.Split(email, "@")
		mx, err := net.LookupMX(parts[1])
		if err != nil || len(mx) == 0 {
			return false
		}
	}
	return true
}

var passwordRegex = regexp.MustCompile(`[a-zA-Z0-9]{3,}`)

func IsPasswordValid(password string) bool {
	return passwordRegex.MatchString(password)
}

func IsFullnameValid(fullname string) bool {
	if len(fullname) < 2 && len(fullname) > 254 {
		return false
	}
	return true
}

func IsTelephoneValid(telephone string) bool {
	if len(telephone) < 6 && len(telephone) > 254 {
		return false
	}
	return true
}
