package errorc

import "errors"

var (
	ErrUserRegistrationFailed = errors.New("User registration failed")
	ErrUserProfileEditFailed  = errors.New("User profile edit failed")
	ErrUserAlreadyRegistered  = errors.New("User already registered")
	ErrUserEmailDuplicated    = errors.New("User email duplicated")
	ErrUserEmailNotValid      = errors.New("Email not valid")
	ErrUserEmailNotVerified   = errors.New("Email not verified")
	ErrAuthFailed             = errors.New("Authentication failed")
	ErrAuthNotLogin           = errors.New("Email or Password is wrong")
	ErrAuthPasswordIncorrect  = errors.New("password is incorrect")
	ErrUserPasswordNotValid   = errors.New("Password not valid. At least 3 character")
	ErrUserNotFound           = errors.New("username entered does not exist")
	ErrUserNotFoundByID       = errors.New("User does not exist")
	ErrUserFullnameNotValid   = errors.New("Fullname not valid")
	ErrUserTelephoneNotValid  = errors.New("Telephone not valid")
	ErrSystem                 = errors.New("Unable to perform the operation")
	ErrSessionDuplicated      = errors.New("SessionDuplicated")
	ErrSessionNotFound        = errors.New("SessionNotFound")
	ErrInvalidRequestMethod   = errors.New("Invalid request method")
)
