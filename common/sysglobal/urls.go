package sysglobal

var (
	UrlIndex            string = "/"
	UrlFormSignup       string = "/signup"
	UrlFormProfile      string = "/profile"
	UrlOauthGoogleLogin string = "/oauth/google"
	UrlOauthCallback    string = "/oauth/callback"

	UrlApiSignup  string = "/api/signup"
	UrlApiProfile string = "/api/profile"
	UrlApiLogin   string = "/api/login"
	UrlApiLogout  string = "/api/logout"
)
