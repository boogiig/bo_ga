package sysglobal

import "boga.me/persistence/sqlstore/sqlrepository"

var (
	GlProcessName string
	GlRunMode     string = "DEV"
	GlLogLevel    string = "debug"
	GlDB          sqlrepository.Container
)
