package logc

import (
	"fmt"
	"io"
	"os"
	"strings"
	"sync"
	"time"

	"boga.me/common/callerutil"
)

const logChanBufferSize = 2048

var exitHandler = func() { os.Exit(-1) }

// Level represents log level type.
type Level int

const (
	// DebugLevel represents DEBUG log level.
	DebugLevel Level = iota

	// InfoLevel represents INFO log level.
	InfoLevel

	// WarningLevel represents WARNING log level.
	WarningLevel

	// ErrorLevel represents ERROR log level.
	ErrorLevel

	// PanicLevel represents FATAL log level.
	PanicLevel

	// FatalLevel represents FATAL log level.
	FatalLevel

	// OffLevel represents a disabledLogger log level.
	OffLevel
)

// Logger represents a common logger interface.
type Logger interface {
	io.Closer

	Level() Level
	Log(level Level, pkg string, file string, line int, format string, args ...interface{})
}

// Debugf writes a 'debug' message to configured logger.
func Debugf(format string, args ...interface{}) {
	if inst := instance(); inst.Level() <= DebugLevel {
		ci := callerutil.GetCallerInfo()
		inst.Log(DebugLevel, ci.Pkg, ci.Filename, ci.Line, format, args...)
	}
}

// Infof writes a 'info' message to configured logger.
func Infof(format string, args ...interface{}) {
	if inst := instance(); inst.Level() <= InfoLevel {
		ci := callerutil.GetCallerInfo()
		inst.Log(InfoLevel, ci.Pkg, ci.Filename, ci.Line, format, args...)
	}
}

// Warnf writes a 'warning' message to configured logger.
func Warnf(format string, args ...interface{}) {
	if inst := instance(); inst.Level() <= WarningLevel {
		ci := callerutil.GetCallerInfo()
		inst.Log(WarningLevel, ci.Pkg, ci.Filename, ci.Line, format, args...)
	}
}

// Errorf writes an 'error' message to configured logger.
func Errorf(format string, args ...interface{}) {
	if inst := instance(); inst.Level() <= ErrorLevel {
		ci := callerutil.GetCallerInfo()
		inst.Log(ErrorLevel, ci.Pkg, ci.Filename, ci.Line, format, args...)
	}
}

// Panicf writes a 'panic' message to configured logger.
// Application should terminate after logging.
func Panicf(format string, args ...interface{}) {
	if inst := instance(); inst.Level() <= PanicLevel {
		ci := callerutil.GetCallerInfo()
		inst.Log(PanicLevel, ci.Pkg, ci.Filename, ci.Line, format, args...)
	}
	return
}

// Fatalf writes a 'fatal' message to configured logger.
// Application should terminate after logging.
func Fatalf(format string, args ...interface{}) {
	if inst := instance(); inst.Level() <= FatalLevel {
		ci := callerutil.GetCallerInfo()
		inst.Log(FatalLevel, ci.Pkg, ci.Filename, ci.Line, format, args...)
	}
	return
}

var (
	instMu sync.RWMutex
	inst   Logger
)

// Disabled stores a disabled logger instance.
var Disabled Logger = &disabledLogger{}

func init() {
	inst = Disabled
}

// Set sets the global logger.
func Set(logger Logger) {
	instMu.Lock()
	_ = inst.Close()
	inst = logger
	instMu.Unlock()
}

// Unset disables a previously set global logger.
func Unset() {
	Set(Disabled)
}

func instance() Logger {
	instMu.RLock()
	l := inst
	instMu.RUnlock()
	return l
}

type record struct {
	level      Level
	pkg        string
	file       string
	line       int
	log        string
	continueCh chan struct{}
}

type logger struct {
	level  Level
	output io.Writer
	files  []io.WriteCloser
	recCh  chan record
}

// New returns a default logger instance.
func New(level string, output io.Writer, files ...io.WriteCloser) (Logger, error) {
	lvl, err := LevelFromString(level)
	if err != nil {
		return nil, err
	}
	l := &logger{
		level:  lvl,
		output: output,
		files:  files,
	}
	l.recCh = make(chan record, logChanBufferSize)
	go l.loop()
	return l, nil
}

func (l *logger) Level() Level {
	return l.level
}

func (l *logger) Log(level Level, pkg string, file string, line int, format string, args ...interface{}) {
	entry := record{
		level:      level,
		pkg:        pkg,
		file:       file,
		line:       line,
		log:        fmt.Sprintf(format, args...),
		continueCh: make(chan struct{}),
	}
	select {
	case l.recCh <- entry:
		if level == FatalLevel || level == PanicLevel {
			<-entry.continueCh // wait until done
		}
	default:
		break // avoid blocking...
	}
}
func (l *logger) Close() error {
	close(l.recCh)
	return nil
}

func (l *logger) loop() {
	for {
		select {
		case rec, ok := <-l.recCh:
			if !ok {
				// close log files
				for _, w := range l.files {
					_ = w.Close()
				}
				return
			}

			line := fmt.Sprintf("%s %s [%s/%s:%d] %s\n", time.Now().Format("2006-01-02 15:04:05.000-07:00"),
				LogLevelAbbreviation(rec.level), rec.pkg, rec.file, rec.line, rec.log)
			_, _ = fmt.Fprint(l.output, line)
			for _, w := range l.files {
				_, _ = fmt.Fprint(w, line)
			}
			if rec.level == FatalLevel || rec.level == PanicLevel {
				exitHandler()
			}
			close(rec.continueCh)
		}
	}
}

func LogLevelAbbreviation(level Level) string {
	switch level {
	case DebugLevel:
		return "DEBUG"
	case InfoLevel:
		return "INFO"
	case WarningLevel:
		return "WARN"
	case ErrorLevel:
		return "ERROR"
	case PanicLevel:
		return "PANIC"
	case FatalLevel:
		return "FATAL"
	default:
		return "OFF"
	}
}

func LogLevelGlyph(level Level) string {
	switch level {
	case DebugLevel:
		return "\U0001f50D"
	case InfoLevel:
		return "\u2139\ufe0f"
	case WarningLevel:
		return "\u26a0\ufe0f"
	case ErrorLevel:
		return "\U0001f4a5"
	case PanicLevel:
		return "\U0001f480"
	case FatalLevel:
		return "\U0001f480"
	default:
		return ""
	}
}

func LevelFromString(level string) (Level, error) {
	switch strings.ToLower(level) {
	case "debug":
		return DebugLevel, nil
	case "", "info":
		return InfoLevel, nil
	case "warning":
		return WarningLevel, nil
	case "error":
		return ErrorLevel, nil
	case "panic":
		return PanicLevel, nil
	case "fatal":
		return FatalLevel, nil
	case "off":
		return OffLevel, nil
	}
	return Level(-1), fmt.Errorf("log: unrecognized level: %s", level)
}
