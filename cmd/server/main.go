package main

import (
	"log"
	"os"

	"boga.me/app/userapp"
)

func main() {
	app := userapp.New(os.Stdout)
	if err := app.Run(); err != nil {
		log.Fatal(err)
	}
}
