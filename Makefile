
DOCKER_IMAGE_TAG ?= test
COLOR := "\e[1;36m%s\e[0m\n"

build-server:
	go mod tidy
	go build -o bo-ga-server ./cmd/server/main.go
start-compose:
	docker-compose up -d
stop-compose:
	docker-compose down