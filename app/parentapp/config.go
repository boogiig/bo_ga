package parentapp

import (
	"bytes"
	"flag"
	"fmt"

	"boga.me/common/sysglobal"
	"boga.me/persistence/sqlstore"
	"boga.me/persistence/sqlstore/sqlrepository"
	"golang.org/x/oauth2"
)

// AppConfig represents a application configuration.
type AppConfig struct {
	GeneralConfig AppGeneralConfig     `yaml:"app"`
	DBConfig      sqlrepository.Config `yaml:"db"`
	OauthConfig   oauth2.Config        `yaml:"oauth"`
}

// GeneralConfig represents a application configuration.
type AppGeneralConfig struct {
	RunMode       string `yaml:"runmode"`
	LogLevel      string `yaml:"loglevel"`
	HTTPPort      string `yaml:"httpport"`
	SessionExpire string `yaml:"sexpire"`
}

// FromArgs loads default application configuration from a specified args.
func (cfg *AppConfig) FromArgs() {
	cfg.DBConfig = *sqlstore.NewDefaultConfig("MYSQL")
	flag.StringVar(&cfg.GeneralConfig.RunMode, "mode", "DEV", "App run mode")
	flag.StringVar(&cfg.GeneralConfig.LogLevel, "loglevel", "debug", "App log level")
	flag.StringVar(&cfg.GeneralConfig.HTTPPort, "httpport", "8080", "App http port")
	flag.StringVar(&cfg.DBConfig.Type, "dbtype", "MYSQL", "DB type")
	flag.StringVar(&cfg.DBConfig.Host, "dbhost", "localhost", "DB host")
	flag.StringVar(&cfg.DBConfig.Port, "dbport", "3306", "DB port")
	flag.StringVar(&cfg.DBConfig.Database, "dbname", "bogadb", "DB name")
	flag.StringVar(&cfg.DBConfig.User, "dbuser", "root", "DB user")
	flag.StringVar(&cfg.DBConfig.Password, "dbpass", "root", "DB pass")
	flag.StringVar(&cfg.OauthConfig.RedirectURL, "oredirect", fmt.Sprintf("http://localhost:8080%s", sysglobal.UrlOauthCallback), "Oauth redirect url")
	flag.StringVar(&cfg.GeneralConfig.SessionExpire, "sexpire", "30", "Session expire timeout unit is (Minute)")
	flag.Parse()

}

// FromFile loads default application configuration from a specified file.
func (cfg *AppConfig) FromFile(configFile string) error {
	return nil
}

// FromBuffer loads default application configuration from a specified byte buffer.
func (cfg *AppConfig) FromBuffer(buf *bytes.Buffer) error {
	return nil
}
