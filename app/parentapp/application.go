package parentapp

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/http/pprof"
	"os"
	"os/signal"
	"syscall"
	"time"

	"boga.me/common/logc"
	"boga.me/common/sysglobal"
)

// Application encapsulates a server application.
type Application struct {
	Config           *AppConfig
	Output           io.Writer
	STDLogger        logc.Logger
	WaitStopCh       chan os.Signal
	ShutDownWaitSecs time.Duration
}

const (
	pprofAddr string = ":7890"
)

func New(output io.Writer) *Application {
	return &Application{
		Config:     &AppConfig{},
		Output:     output,
		WaitStopCh: make(chan os.Signal, 1)}
}

// Run runs application until either a stop signal is received or an error occurs.
func (a *Application) Run() error {
	return fmt.Errorf("Not Implemented")
}

// Configure application
func (a *Application) ConfigureApp() {
	a.Config.FromArgs()
	sysglobal.GlRunMode = a.Config.GeneralConfig.RunMode
	sysglobal.GlLogLevel = a.Config.GeneralConfig.LogLevel
}

// InitLogger : Log configure
func (a *Application) InitLogger(output io.Writer) error {
	var logFiles []io.WriteCloser
	l, lerr := logc.New(sysglobal.GlLogLevel, output, logFiles...)
	if lerr != nil {
		return lerr
	}
	logc.Set(l)
	a.STDLogger = l
	return nil
}

// SignalQuitWait : Gracefully shutdown
func (a *Application) SignalQuitWait(srv *http.Server) {
	logc.Infof("Server started listen http: %s", srv.Addr)
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	logc.Infof("Shutting down server...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		logc.Fatalf("Server forced to shutdown: %s", err)
	}
}

func (a *Application) StartHTTPDebuger() {
	pprofHandler := http.NewServeMux()
	pprofHandler.Handle("/debug/pprof/", http.HandlerFunc(pprof.Index))
	server := &http.Server{Addr: pprofAddr, Handler: pprofHandler}
	go server.ListenAndServe()
}
