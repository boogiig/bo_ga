package middleware

import (
	"net/http"
	"time"

	"boga.me/common/logc"
)

func HandlerLogging(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		handler.ServeHTTP(w, r)
		duration := time.Since(start)
		logc.Debugf("%s %s %s [%v]", r.RemoteAddr, r.Method, r.URL, duration)
	})
}
