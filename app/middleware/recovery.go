package middleware

import (
	"net/http"

	"boga.me/common/errorc"
	"boga.me/common/httpc"
	"boga.me/common/logc"
)

func HandlerRecovery(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		defer func() {
			err := recover()
			if err != nil {
				logc.Errorf("HTTP recover error: %v", err)
				httpc.ResponseError(w, r, http.StatusInternalServerError, errorc.ErrSystem.Error())
			}

		}()

		next.ServeHTTP(w, r)

	})
}
