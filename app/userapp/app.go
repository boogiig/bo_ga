package userapp

import (
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	"boga.me/app/middleware"
	"boga.me/app/parentapp"
	"boga.me/app/sessionmanager"
	"boga.me/app/userapp/userapi"
	"boga.me/common/logc"
	"boga.me/common/sysglobal"
	"boga.me/persistence/sqlstore"
	"golang.org/x/oauth2"
)

type Application struct {
	*parentapp.Application
	Web          *userapi.WebController
	SManager     *sessionmanager.SessionManager
	OauthManager *userapi.OauthManager
	UserManager  *userapi.UserManager
}

// New returns a runnable application given an output and a command line arguments array.
func New(output io.Writer) *Application {
	return &Application{
		Application: parentapp.New(output),
	}
}

func (a *Application) Run() error {
	a.ConfigureApp()
	var err error
	err = a.InitLogger(a.Output)
	if err != nil {
		return err
	}
	sysglobal.GlDB, err = sqlstore.New(&a.Config.DBConfig)
	if err != nil {
		logc.Errorf("DB Init error:%v", err)
		return err
	}
	sessionExpire, err := strconv.Atoi(a.Config.GeneralConfig.SessionExpire)
	if err != nil {
		logc.Errorf("SessionExpire parse error:%v", err)
		return err
	}
	a.SManager = sessionmanager.New(time.Duration(sessionExpire)*time.Minute, sysglobal.GlDB.Session(), sysglobal.GlDB.User())
	a.SManager.SessionErrorHandle = userapi.RedirectWithError
	a.Web = userapi.NewWebController(a.SManager)
	a.Config.OauthConfig.ClientID = "20777428271-frstg9mejfs8k3ap2gus2ve44tit6934.apps.googleusercontent.com"
	a.Config.OauthConfig.ClientSecret = "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"
	a.Config.OauthConfig.Scopes = []string{"email", "profile"}
	a.Config.OauthConfig.Endpoint = oauth2.Endpoint{
		AuthURL:   "https://accounts.google.com/o/oauth2/auth",
		TokenURL:  "https://oauth2.googleapis.com/token",
		AuthStyle: oauth2.AuthStyleInParams,
	}
	a.UserManager = userapi.NewUserManager(a.SManager)
	a.OauthManager = userapi.NewOauthManager(&a.Config.OauthConfig, "https://accounts.google.com", a.SManager)
	a.OauthManager.ErrorHandle = userapi.RedirectWithError
	logc.Debugf("AppConfig: %+v", a.Config)

	a.SManager.StartCleanup()

	mux := http.NewServeMux()
	mux.Handle("/public/", http.StripPrefix("/public", http.FileServer(http.Dir("./app/userapp/userweb/public"))))
	http.Handle("/favicon.ico", http.NotFoundHandler())
	mux.HandleFunc(sysglobal.UrlIndex, a.Web.Index)
	mux.HandleFunc(sysglobal.UrlOauthGoogleLogin, a.OauthManager.HandleLogin)
	mux.HandleFunc(sysglobal.UrlOauthCallback, a.OauthManager.HandleCallback)
	mux.HandleFunc(sysglobal.UrlFormSignup, a.Web.SignUp)
	mux.HandleFunc(sysglobal.UrlFormProfile, a.Web.Profile)
	mux.HandleFunc(sysglobal.UrlApiSignup, a.UserManager.CreateUser)
	mux.HandleFunc(sysglobal.UrlApiProfile, a.UserManager.EditUser)
	mux.HandleFunc(sysglobal.UrlApiLogin, a.UserManager.Login)
	mux.HandleFunc(sysglobal.UrlApiLogout, a.UserManager.Logout)

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.Config.GeneralConfig.HTTPPort),
		Handler: middleware.HandlerRecovery(a.SManager.HandlerSession(mux)),
	}

	if sysglobal.GlRunMode != "PROD" {
		srv.Handler = middleware.HandlerLogging(srv.Handler)
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			logc.Fatalf("ListenAndServe error: %v", err)
		}
	}()
	a.SignalQuitWait(srv)
	a.SManager.StopCleanup()
	logc.Unset()
	return nil
}
