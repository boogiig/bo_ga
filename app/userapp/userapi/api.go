package userapi

import (
	"fmt"
	"net/http"

	"boga.me/app/sessionmanager"
	"boga.me/common/errorc"
	"boga.me/common/hashc"
	"boga.me/common/sysglobal"
	"boga.me/common/validation"
	"boga.me/persistence/persistmodel"
)

type UserManager struct {
	sessionManager *sessionmanager.SessionManager
}

func NewUserManager(sessionManager *sessionmanager.SessionManager) *UserManager {
	m := &UserManager{
		sessionManager: sessionManager,
	}
	return m
}

func (m *UserManager) Login(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		email := r.FormValue("email")
		password := r.FormValue("password")
		err := validation.ValidateUserParams(email, password)
		if err != nil {
			RedirectWithError(w, r, sysglobal.UrlIndex, err, http.StatusMovedPermanently)
			return
		}
		user, err := sysglobal.GlDB.User().FetchUserByEmail(r.Context(), email)
		if err != nil {
			RedirectWithError(w, r, sysglobal.UrlIndex, err, http.StatusMovedPermanently)
			return
		}
		if user.IsOauth {
			RedirectWithError(w, r, sysglobal.UrlIndex, errorc.ErrUserNotFound, http.StatusMovedPermanently)
			return
		}
		if user.Password != hashc.SHA1String(password) {
			RedirectWithError(w, r, sysglobal.UrlIndex, errorc.ErrAuthPasswordIncorrect, http.StatusMovedPermanently)
			return
		}
		m.sessionManager.CreateAuthCtx(r.Context(), user)
		RedirectWithMessage(w, r, sysglobal.UrlIndex, "Successfully logged in", http.StatusMovedPermanently)
	}
}

func (m *UserManager) Logout(w http.ResponseWriter, r *http.Request) {
	sd := m.sessionManager.GetSessionDataFromContext(r.Context())
	sd.LoggedIn = false
	if sd.Session.SessionID != "" {
		m.sessionManager.Store.DeleteSession(r.Context(), sd.Session.SessionID)
	}
	http.Redirect(w, r, sysglobal.UrlIndex, http.StatusMovedPermanently)
}

func (m *UserManager) CreateUser(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		email := r.FormValue("email")
		password := r.FormValue("password")
		err := validation.ValidateUserParams(email, password)
		if err != nil {
			RedirectWithError(w, r, sysglobal.UrlFormSignup, err, http.StatusMovedPermanently)
			return
		}
		exists, err := sysglobal.GlDB.User().UserExists(r.Context(), email)
		if err != nil {
			RedirectWithError(w, r, sysglobal.UrlFormSignup, err, http.StatusMovedPermanently)
			return
		}
		if exists {
			RedirectWithError(w, r, sysglobal.UrlFormSignup, errorc.ErrUserAlreadyRegistered, http.StatusMovedPermanently)
			return
		}
		user, err := sysglobal.GlDB.User().CreateUserByEmailPassword(r.Context(), email, hashc.SHA1String(password), false, false)
		if err != nil {
			RedirectWithError(w, r, sysglobal.UrlFormSignup, err, http.StatusMovedPermanently)
			return
		}
		m.sessionManager.CreateAuthCtx(r.Context(), user)
		RedirectWithMessage(w, r, sysglobal.UrlFormProfile, "Successfully registered", http.StatusMovedPermanently)
	} else {
		http.NotFound(w, r)
	}
}

func (m *UserManager) EditUser(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		email := r.FormValue("email")
		fullname := r.FormValue("fullname")
		telephone := r.FormValue("telephone")
		emailLen := len(email)
		if emailLen > 0 && !validation.IsEmailValid(email, false) {
			RedirectWithError(w, r, sysglobal.UrlFormProfile, errorc.ErrUserEmailNotValid, http.StatusMovedPermanently)
			return
		}
		err := validation.ValidateUserProfileParams(fullname, telephone)
		if err != nil {
			RedirectWithError(w, r, sysglobal.UrlFormProfile, err, http.StatusMovedPermanently)
			return
		}
		userID := m.sessionManager.GetUserID(r.Context())
		user, err := sysglobal.GlDB.User().FetchUserByID(r.Context(), userID)
		if err != nil {
			RedirectWithError(w, r, sysglobal.UrlFormProfile, err, http.StatusMovedPermanently)
			return
		}

		editEmail := email
		if user.IsOauth || emailLen == 0 {
			editEmail = user.Email
		}

		updated := &persistmodel.User{ID: userID, Email: editEmail, Fullname: fullname, Telephone: telephone, ProfileOk: true}
		err = sysglobal.GlDB.User().UpdateUser(r.Context(), updated)
		if err != nil {
			RedirectWithError(w, r, sysglobal.UrlFormProfile, err, http.StatusMovedPermanently)
			return
		}
		RedirectWithMessage(w, r, sysglobal.UrlIndex, "Profile successfully changed", http.StatusMovedPermanently)
	} else {
		http.NotFound(w, r)
	}
}

func RedirectWithError(w http.ResponseWriter, r *http.Request, url string, err error, status int) {
	http.Redirect(w, r, fmt.Sprintf("%s?msg=%s", url, err.Error()), status)
}

func RedirectWithMessage(w http.ResponseWriter, r *http.Request, url string, message string, status int) {
	http.Redirect(w, r, fmt.Sprintf("%s?msg=%s", url, message), status)
}
