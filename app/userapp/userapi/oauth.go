package userapi

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"net/http"
	"time"

	"boga.me/app/sessionmanager"
	"boga.me/common/errorc"
	"boga.me/common/logc"
	"boga.me/common/sysglobal"
	"boga.me/persistence/persistmodel"
	"github.com/coreos/go-oidc/v3/oidc"
	"golang.org/x/oauth2"
)

type OauthManager struct {
	config         *oauth2.Config
	ErrorHandle    func(http.ResponseWriter, *http.Request, string, error, int)
	provider       *oidc.Provider
	sessionManager *sessionmanager.SessionManager
}

type User struct {
	OAuth2Token *oauth2.Token
	IDToken     *Claims
}

type Claims struct {
	Email         string `json:"email"`
	EmailVerified bool   `json:"email_verified"`
	Name          string `json:"name"`
}

func NewOauthManager(config *oauth2.Config, providerUrl string, sessionManager *sessionmanager.SessionManager) *OauthManager {
	o := &OauthManager{
		config:         config,
		sessionManager: sessionManager,
	}
	var err error
	o.provider, err = oidc.NewProvider(context.Background(), providerUrl)
	if err != nil {
		logc.Fatalf("OIDC Provider error:%v", err)
	}
	return o
}

func (o *OauthManager) HandleLogin(w http.ResponseWriter, r *http.Request) {
	state, err := randString()
	if err != nil {
		logc.Errorf("state randString error: %v", err)
		o.ErrorHandle(w, r, sysglobal.UrlIndex, errorc.ErrAuthFailed, http.StatusMovedPermanently)
		return
	}
	nonce, err := randString()
	if err != nil {
		logc.Errorf("nonce randString error: %v", err)
		o.ErrorHandle(w, r, sysglobal.UrlIndex, errorc.ErrAuthFailed, http.StatusMovedPermanently)
		return
	}
	setCallbackCookie(w, r, "state", state)
	setCallbackCookie(w, r, "nonce", nonce)
	http.Redirect(w, r, o.config.AuthCodeURL(state, oauth2.SetAuthURLParam("nonce", nonce)), http.StatusMovedPermanently)
}

func (o *OauthManager) HandleCallback(w http.ResponseWriter, r *http.Request) {
	oauthuser, status, err := o.exchangeCode(w, r, o.config, o.provider)
	if err != nil {
		logc.Errorf("Oauth callback error: %v, status: %v", err, status)
		o.ErrorHandle(w, r, sysglobal.UrlIndex, errorc.ErrAuthFailed, http.StatusMovedPermanently)
		return
	}
	if oauthuser != nil && oauthuser.IDToken != nil && !oauthuser.IDToken.EmailVerified {
		o.ErrorHandle(w, r, sysglobal.UrlIndex, errorc.ErrUserEmailNotVerified, http.StatusMovedPermanently)
		return
	}
	exists, err := sysglobal.GlDB.User().UserExists(r.Context(), oauthuser.IDToken.Email)
	if err != nil {
		o.ErrorHandle(w, r, sysglobal.UrlIndex, err, http.StatusMovedPermanently)
		return
	}
	var user *persistmodel.User
	if !exists {
		user, err = sysglobal.GlDB.User().CreateUserByEmailName(r.Context(), oauthuser.IDToken.Email, oauthuser.IDToken.Name, false, true)
		if err != nil {
			o.ErrorHandle(w, r, sysglobal.UrlIndex, err, http.StatusMovedPermanently)
			return
		}
	} else {
		user, err = sysglobal.GlDB.User().FetchUserByEmail(r.Context(), oauthuser.IDToken.Email)
		if err != nil {
			o.ErrorHandle(w, r, sysglobal.UrlIndex, err, http.StatusMovedPermanently)
			return
		}
	}
	o.sessionManager.CreateAuthCtx(r.Context(), user)
	returnUrl := r.Header.Get("Referer")
	if returnUrl == "" {
		returnUrl = "/"
	}
	http.Redirect(w, r, returnUrl, http.StatusMovedPermanently)
}

func (o *OauthManager) exchangeCode(w http.ResponseWriter, r *http.Request, config *oauth2.Config, provider *oidc.Provider) (*User, int, error) {
	state, err := r.Cookie("state")
	if err != nil {
		return nil, http.StatusBadRequest, errors.New("State cookie is not set in request")
	}
	if r.URL.Query().Get("state") != state.Value {
		return nil, http.StatusBadRequest, errors.New("State cookie did not match")
	}

	oauth2Token, err := o.config.Exchange(r.Context(), r.URL.Query().Get("code"))
	if err != nil {
		return nil, http.StatusInternalServerError, errors.New("Unable to exchange token: " + err.Error())
	}

	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		return nil, http.StatusInternalServerError, errors.New("No id_token field in oauth2 token.")
	}
	oidcConfig := &oidc.Config{
		ClientID: config.ClientID,
	}
	verifier := provider.Verifier(oidcConfig)
	idToken, err := verifier.Verify(r.Context(), rawIDToken)
	if err != nil {
		return nil, http.StatusInternalServerError, errors.New("Unable to verify ID Token: " + err.Error())
	}

	nonce, err := r.Cookie("nonce")
	if err != nil {
		return nil, http.StatusBadRequest, errors.New("Nonce is not provided")
	}
	if idToken.Nonce != nonce.Value {
		return nil, http.StatusBadRequest, errors.New("Nonce did not match")
	}

	var claims Claims

	if err := idToken.Claims(&claims); err != nil {
		return nil, http.StatusInternalServerError, errors.New(err.Error())
	}

	user := User{
		OAuth2Token: oauth2Token,
		IDToken:     &claims,
	}

	return &user, 0, nil
}

func setCallbackCookie(w http.ResponseWriter, r *http.Request, name, value string) {
	cookie := &http.Cookie{
		Name:     name,
		Value:    value,
		MaxAge:   int(time.Hour.Seconds()),
		Secure:   r.TLS != nil,
		HttpOnly: true,
	}
	http.SetCookie(w, cookie)
}

func randString() (string, error) {
	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return base64.RawURLEncoding.EncodeToString(b), nil
}
