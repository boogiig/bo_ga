package userapi

import (
	"html/template"
	"net/http"

	"boga.me/app/sessionmanager"
	"boga.me/common/sysglobal"
)

type WebController struct {
	tpl            *template.Template
	sessionManager *sessionmanager.SessionManager
}

func NewWebController(sessionManager *sessionmanager.SessionManager) *WebController {
	c := &WebController{sessionManager: sessionManager}
	c.tpl = template.Must(template.ParseGlob("./app/userapp/userweb/html/*.html"))
	return c
}

func (c *WebController) Index(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != sysglobal.UrlIndex {
		http.Redirect(w, r, sysglobal.UrlIndex, http.StatusMovedPermanently)
		return
	}
	c.ServeSessionTemplate(w, r, "home.html")
}

func (c *WebController) SignUp(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != sysglobal.UrlFormSignup {
		http.Redirect(w, r, sysglobal.UrlIndex, http.StatusMovedPermanently)
		return
	}
	if c.sessionManager.IsSessionAuthenticated(w, r) {
		http.Redirect(w, r, sysglobal.UrlIndex, http.StatusMovedPermanently)
		return
	}
	c.ServeSessionTemplate(w, r, "signup.html")
}

func (c *WebController) Profile(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != sysglobal.UrlFormProfile {
		http.Redirect(w, r, sysglobal.UrlIndex, http.StatusMovedPermanently)
		return
	}
	if !c.sessionManager.IsSessionAuthenticated(w, r) {
		http.Redirect(w, r, sysglobal.UrlIndex, http.StatusMovedPermanently)
		return
	}
	c.ServeSessionTemplate(w, r, "profile.html")
}

func (c *WebController) ServeSessionTemplate(w http.ResponseWriter, r *http.Request, templateName string) {
	sd := c.sessionManager.GetSessionDataFromContext(r.Context())
	c.tpl.ExecuteTemplate(w, templateName, &sd)
}
