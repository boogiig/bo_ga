package sessionmanager

import (
	"context"
	"net/http"
	"time"

	"boga.me/common/logc"
	"boga.me/common/sysglobal"
	"boga.me/persistence/persistmodel"
	"boga.me/persistence/sqlstore/sqlrepository"
)

type SessionManager struct {
	Lifetime           time.Duration
	Store              sqlrepository.Session
	UserStore          sqlrepository.User
	contextKey         string
	Cookie             CookieConfig
	SessionErrorHandle func(http.ResponseWriter, *http.Request, string, error, int)
	CleanInterval      time.Duration
	stopClean          chan bool
}

type CookieConfig struct {
	Name     string
	Domain   string
	HttpOnly bool
	Path     string
	Persist  bool
	SameSite http.SameSite
	Secure   bool
}

func New(lifetime time.Duration, store sqlrepository.Session, user sqlrepository.User) *SessionManager {
	s := &SessionManager{
		Lifetime:      lifetime,
		CleanInterval: 30 * time.Second,
		Store:         store,
		UserStore:     user,
		contextKey:    "sctx",
		Cookie: CookieConfig{
			Name:     "session",
			Domain:   "",
			HttpOnly: true,
			Path:     "/",
			Persist:  true,
			Secure:   false,
			SameSite: http.SameSiteLaxMode,
		},
	}
	return s
}

func (s *SessionManager) HandlerSession(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var sessionID string
		cookie, err := r.Cookie(s.Cookie.Name)
		if err == nil {
			sessionID = cookie.Value
		}

		ctx, err := s.LoadSession(r.Context(), sessionID)
		if err != nil {
			s.SessionErrorHandle(w, r, sysglobal.UrlIndex, err, http.StatusMovedPermanently)
			return
		}

		sr := r.WithContext(ctx)
		bw := &bufferedResponseWriter{ResponseWriter: w}
		next.ServeHTTP(bw, sr)

		sd := s.GetSessionDataFromContext(ctx)
		logc.Debugf("SD: %+v", sd)
		logc.Debugf("HandlerSession SD: %+v, session:%+v", sd, sd.Session)
		if sd.LoggedIn {
			if !sd.Persisted {
				sessionID, expiry, err := s.CreateSession(ctx)
				if err != nil {
					s.SessionErrorHandle(w, r, sysglobal.UrlIndex, err, http.StatusMovedPermanently)
					return
				}
				s.WriteSessionCookie(ctx, w, sessionID, expiry)
			}

		} else {
			s.WriteSessionCookie(ctx, w, "", time.Time{})
		}
		w.Header().Add("Vary", "Cookie")

		if bw.code != 0 {
			w.WriteHeader(bw.code)
		}
		w.Write(bw.buf.Bytes())
	})
}

func (s *SessionManager) CreateAuthCtx(ctx context.Context, user *persistmodel.User) {
	sd := s.GetSessionDataFromContext(ctx)

	sd.mu.Lock()
	s.setUser(sd, user)
	sd.mu.Unlock()

	logc.Debugf("CreateAuthCtx SD: %+v, session:%+v", sd, sd.Session)
}

func (s *SessionManager) GetUserID(ctx context.Context) int64 {
	sd := s.GetSessionDataFromContext(ctx)
	return sd.Session.UserID
}

func (s *SessionManager) IsSessionAuthenticated(w http.ResponseWriter, r *http.Request) bool {
	sd := s.GetSessionDataFromContext(r.Context())
	return sd.LoggedIn
}

func (s *SessionManager) IsSessionProfileOK(w http.ResponseWriter, r *http.Request) bool {
	sd := s.GetSessionDataFromContext(r.Context())
	return sd.User.ProfileOk
}

func (s *SessionManager) setUser(sd *SessionData, user *persistmodel.User) {
	sd.User = *user
	sd.Session.UserID = sd.User.ID
	sd.LoggedIn = true
}

func (s *SessionManager) WriteSessionCookie(ctx context.Context, w http.ResponseWriter, sessionID string, expiry time.Time) {
	cookie := &http.Cookie{
		Name:     s.Cookie.Name,
		Value:    sessionID,
		Path:     s.Cookie.Path,
		Domain:   s.Cookie.Domain,
		Secure:   s.Cookie.Secure,
		HttpOnly: s.Cookie.HttpOnly,
		SameSite: s.Cookie.SameSite,
	}

	if expiry.IsZero() {
		cookie.Expires = time.Unix(1, 0)
		cookie.MaxAge = -1
	} else if s.Cookie.Persist {
		cookie.Expires = time.Unix(expiry.Unix()+1, 0)
		cookie.MaxAge = int(time.Until(expiry).Seconds() + 1)
	}
	w.Header().Add("Set-Cookie", cookie.String())
	w.Header().Add("Cache-Control", `no-cache="Set-Cookie"`)
	logc.Debugf("WriteSessionCookie: %+v", cookie)
}
