package sessionmanager

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"sync"
	"time"

	"boga.me/common/errorc"
	"boga.me/common/logc"
	"boga.me/persistence/persistmodel"
)

type SessionData struct {
	Session   persistmodel.Session
	User      persistmodel.User
	LoggedIn  bool
	Persisted bool
	mu        sync.Mutex
	Message   MessageData
}

type MessageData struct {
	IsMessage   bool
	TextMessage string
	// TextType      string
}

func NewSessionData(lifetime time.Duration) *SessionData {
	return &SessionData{
		Session:  persistmodel.Session{Expiry: time.Now().Add(lifetime).UTC()},
		LoggedIn: false,
	}
}

func NewSessionDataWithUser(lifetime time.Duration, user persistmodel.User) *SessionData {
	return &SessionData{
		Session:  persistmodel.Session{Expiry: time.Now().Add(lifetime).UTC()},
		User:     user,
		LoggedIn: true,
	}
}

func (s *SessionManager) LoadSession(ctx context.Context, sessionID string) (context.Context, error) {

	if sessionID == "" {
		return s.addSessionDataToContext(ctx, NewSessionData(s.Lifetime)), nil
	}

	session, err := s.Store.FetchSession(ctx, sessionID)
	if err != nil {
		if err == errorc.ErrSessionNotFound {
			return s.addSessionDataToContext(ctx, NewSessionData(s.Lifetime)), nil
		}
		return nil, err
	} else if session == nil {
		return s.addSessionDataToContext(ctx, NewSessionData(s.Lifetime)), nil
	}

	sd := &SessionData{
		LoggedIn: false,
		Session:  *session,
	}

	if session.UserID > -1 {
		user, err := s.UserStore.FetchUserByID(ctx, session.UserID)
		if err != nil {
			return s.addSessionDataToContext(ctx, NewSessionData(s.Lifetime)), nil
		}
		s.setUser(sd, user)
		sd.Persisted = true
	}

	return s.addSessionDataToContext(ctx, sd), nil
}

func (s *SessionManager) CreateSession(ctx context.Context) (string, time.Time, error) {
	sd := s.GetSessionDataFromContext(ctx)

	sd.mu.Lock()
	defer sd.mu.Unlock()

	if sd.Session.SessionID == "" {
		var err error
		if sd.Session.SessionID, err = generateSessionID(); err != nil {
			return "", time.Time{}, err
		}
	}

	expiry := sd.Session.Expiry

	if err := s.Store.CreateSession(ctx, sd.Session.SessionID, sd.Session.UserID, expiry.UTC()); err != nil {
		logc.Errorf("CreateSession DB error: %v", err)
		return "", time.Time{}, err
	}

	return sd.Session.SessionID, expiry, nil
}

func (s *SessionManager) addSessionDataToContext(ctx context.Context, sd *SessionData) context.Context {
	return context.WithValue(ctx, s.contextKey, sd)
}

func (s *SessionManager) GetSessionDataFromContext(ctx context.Context) *SessionData {
	c, ok := ctx.Value(s.contextKey).(*SessionData)
	if !ok {
		return nil
	}
	return c
}

func generateSessionID() (string, error) {
	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return base64.RawURLEncoding.EncodeToString(b), nil
}

func (s *SessionData) NewMessage(text string) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.UnsafeNewMessage(text)
}

func (s *SessionData) UnsafeNewMessage(text string) {
	s.Message.TextMessage = text
	s.Message.IsMessage = len(text) > 0
}
