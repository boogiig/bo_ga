package sessionmanager

import (
	"context"
	"sync"
	"time"

	"boga.me/persistence/persistmodel"
)

type MemCache struct {
	sessions map[string]*persistmodel.Session
	mu       sync.RWMutex
}

func NewMemCache() *MemCache {
	m := &MemCache{
		sessions: make(map[string]*persistmodel.Session),
	}
	return m
}

// CreateSession inserts a new session entity into storage
func (m *MemCache) CreateSession(ctx context.Context, sessionID string, userID int64, expiry time.Time) error {
	m.mu.Lock()
	m.sessions[sessionID] = &persistmodel.Session{
		UserID: userID,
		Expiry: expiry,
	}
	m.mu.Unlock()

	return nil
}

// DeleteSession deletes a session entity from storage.
func (m *MemCache) DeleteSession(ctx context.Context, sessionID string) error {
	m.mu.Lock()
	delete(m.sessions, sessionID)
	m.mu.Unlock()

	return nil
}

// FetchSession retrieves a session entity from storage.
func (m *MemCache) FetchSession(ctx context.Context, sessionID string) (*persistmodel.Session, error) {
	m.mu.RLock()
	defer m.mu.RUnlock()

	session, ok := m.sessions[sessionID]
	if !ok {
		return nil, nil
	}

	if time.Now().UTC().After(session.Expiry.UTC()) {
		return nil, nil
	}
	return session, nil
}

// DeleteExpiredSessions deletes a expired sessions from storage.
func (m *MemCache) DeleteExpiredSessions(ctx context.Context) error {
	return nil
}
