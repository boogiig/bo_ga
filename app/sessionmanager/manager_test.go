package sessionmanager

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"

	"boga.me/common/httpc"
	"boga.me/common/logc"
)

type testServer struct {
	*httptest.Server
}

func newTestServer(t *testing.T, h http.Handler) *testServer {
	ts := httptest.NewTLSServer(h)

	jar, err := cookiejar.New(nil)
	if err != nil {
		t.Fatal(err)
	}
	ts.Client().Jar = jar

	ts.Client().CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}

	return &testServer{ts}
}

func (ts *testServer) execute(t *testing.T, urlPath string) (http.Header, string) {
	rs, err := ts.Client().Get(ts.URL + urlPath)
	if err != nil {
		t.Fatal(err)
	}

	defer rs.Body.Close()
	body, err := ioutil.ReadAll(rs.Body)
	if err != nil {
		t.Fatal(err)
	}

	return rs.Header, string(body)
}

func extractTokenFromCookie(c string) string {
	parts := strings.Split(c, ";")
	return strings.SplitN(parts[0], "=", 2)[1]
}

func InitLogger(output io.Writer) error {
	var logFiles []io.WriteCloser
	l, lerr := logc.New("debug", output, logFiles...)
	if lerr != nil {
		return lerr
	}
	logc.Set(l)
	return nil
}

func TestCookie(t *testing.T) {
	t.Parallel()
	InitLogger(os.Stdout)
	sessionManager := New(10*time.Second, NewMemCache(), nil)

	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		httpc.ResponseDone(w, r, http.StatusOK, "TEST OK")
	})
	ts := newTestServer(t, sessionManager.HandlerSession(mux))
	defer ts.Close()

	header, body := ts.execute(t, "/")
	// t.Logf("HEADER: %q, BODY: %q", header, body)
	token1 := extractTokenFromCookie(header.Get("Set-Cookie"))

	if !strings.Contains(body, "TEST OK") {
		t.Errorf("check %q; get %q", "TEST OK", body)
	}
	if header.Get("Set-Cookie") == "" {
		t.Errorf("check %q; get %q", "", header.Get("Set-Cookie"))
	}

	header, _ = ts.execute(t, "/")
	token2 := extractTokenFromCookie(header.Get("Set-Cookie"))
	if token1 != token2 {
		t.Error("Check tokens to be the same")
	}
}
