package sessionmanager

import (
	"context"
	"time"

	"boga.me/common/logc"
)

func (s *SessionManager) StartCleanup() {
	s.stopClean = make(chan bool)
	go func() {
		ticker := time.NewTicker(s.CleanInterval)
		for {
			logc.Debugf("DeleteExpiredSessions")
			err := s.Store.DeleteExpiredSessions(context.Background())
			if err != nil {
				logc.Errorf("DeleteExpiredSessions error: %v", err)
			}
			select {
			case <-s.stopClean:
				ticker.Stop()
				return
			case <-ticker.C:
			}
		}
	}()

}

func (s *SessionManager) StopCleanup() {
	if s.stopClean != nil {
		s.stopClean <- true
	}
}
