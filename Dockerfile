FROM golang:1.17-alpine3.14 AS go-builder
RUN apk add --update --no-cache \
    make
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN make build-server

FROM alpine:3.14
WORKDIR /root/
COPY --from=go-builder /app/bo-ga-server .
RUN mkdir -p ./app/userapp/userweb
COPY --from=go-builder /app/app/userapp/userweb ./app/userapp/userweb
EXPOSE $PORT
CMD ./bo-ga-server -mode PROD -loglevel info -httpport $PORT -dbtype MYSQL -dbhost us-cdbr-east-05.cleardb.net -dbport 3306 -dbname heroku_55d43dad14c4762 -dbuser b9c03cc5560c5a -dbpass 0e8cbead -oredirect http://bo-ga.herokuapp.com/oauth/callback